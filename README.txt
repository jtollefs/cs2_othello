/* README.txt */

Strategies Used:

Some strategies I would use are:

1. Write a function that iterates through all possible moves on my side. Then, caluclate all possible moves from my opponent after their move.  

2. Assign values to squares on the board. E.g., +8 for getting one of my pieces in the corner, -5 for spaces adjacent to the cornes but on the edge; -6 for the spaces digonally adjacent to the corner. +4 for spaces two away from the corners. +8 for pieces which can not possibly be taken. -i for spaces where i is the number of opponent's moves that could take my piece. Hopefully I could run enough games to assign empirically-tested values. 

3. If possible, iterate to greater depths and use alpha/beta scoring to determine the best possible move. Use iterative deepening to pick the shortest trees in order for the AI to makes its decision in the time limit. Keep track of the highest possible value and the time and after a certain length of time has passed, choose the current best move. 

I think this strategy would work because using trial-and-error to estimate the values will make my AI better suited for play against an opponent who hasn't tested similarly. 

Note: since I worked by myself I didn't write up what everyone in my group did. 
